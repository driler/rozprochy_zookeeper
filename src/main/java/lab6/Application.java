package lab6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Kisiel on 04.06.2017.
 */
public class Application {

    public static void main(String args[]) throws IOException {
        System.out.print("Args: ");
        Arrays.stream(args).forEach(arg -> System.out.print(arg + " "));
        System.out.println("");

        if (args.length == 2) {
            final Executor executor = new Executor("localhost", Integer.parseInt(args[0]), args[1], "/znodeTestowy");

            executor.start();

            final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.equals("q")) {
                    executor.stop();
                    break;
                } else if (line.equals("show"))
                    executor.printNodeStructure();
                else
                    System.out.println("Wrong command");

            }
        }else
            System.out.println("Wrong arguments! \nCorrect: <port> <program_name>");
    }
}
